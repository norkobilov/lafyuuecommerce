# Generated by Django 3.2 on 2022-07-07 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lafyuu', '0012_alter_size_size'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='colour',
            field=models.ManyToManyField(blank=True, null=True, related_name='product', to='lafyuu.Colour'),
        ),
        migrations.AlterField(
            model_name='products',
            name='size',
            field=models.ManyToManyField(blank=True, null=True, related_name='product', to='lafyuu.Size'),
        ),
    ]
