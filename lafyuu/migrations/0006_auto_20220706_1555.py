# Generated by Django 3.2 on 2022-07-06 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lafyuu', '0005_auto_20220702_1718'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('stock', models.CharField(blank=True, max_length=5, null=True)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
            ],
        ),
        migrations.RemoveField(
            model_name='products',
            name='flash_sale',
        ),
        migrations.RemoveField(
            model_name='products',
            name='sale',
        ),
    ]
