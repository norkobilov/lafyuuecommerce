# Generated by Django 3.2 on 2022-07-16 11:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lafyuu', '0021_auto_20220716_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userread',
            name='notification',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_read', to='lafyuu.notificationtype'),
        ),
    ]
