# Generated by Django 3.2 on 2022-07-07 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lafyuu', '0011_products_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='size',
            name='size',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
    ]
