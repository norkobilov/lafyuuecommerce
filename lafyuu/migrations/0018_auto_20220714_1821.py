# Generated by Django 3.2 on 2022-07-14 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lafyuu', '0017_auto_20220713_1837'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='image',
        ),
        migrations.AddField(
            model_name='review',
            name='image',
            field=models.ManyToManyField(to='lafyuu.Image'),
        ),
    ]
