from rest_framework import serializers

from lafyuu.models import *
from django.db.models import Avg
from accounts.api.serializers import UserInfoSerializer


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ["name"]

    # def create(self, validated_data):
    #     return Products.objects.create(**validated_data)


class ProductInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('id', 'name',)


class ProductSerializer(serializers.ModelSerializer):
    size = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    review = serializers.SerializerMethodField()
    sale = serializers.SerializerMethodField()

    class Meta:
        model = Products
        fields = ('id', 'name', 'price', 'image', 'size', 'review', 'sale')

    def get_size(self, obj):
        return SizeSerializer(obj.size, many=True, context={"request": self.context["request"]}).data

    def get_sale(self, obj):
        return SaleSerializer(obj.sale, context={"request": self.context["request"]}).data

    def get_image(self, obj):
        images = Image.objects.filter(product=obj).all()
        return ImageSerializer(images, many=True).data

    def get_review(self, obj):
        review = Review.objects.filter(product=obj).aggregate(Avg('stars'))
        return review
        # return ReviewSerializer(review, many=True).data


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ['size']


class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = ['name']


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image', 'product']


class ImageReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class FavoriteSerializer(serializers.ModelSerializer):
    product = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Favorite
        fields = ('product', 'user')

    def get_product(self, obj):
        product = obj.product
        return ProductInfoSerializer(product, many=False, context={"request": self.context["request"]}).data

    def get_user(self, obj):
        user = obj.user
        return UserInfoSerializer(user, many=False, context={"request": self.context["request"]}).data


class FavoriteInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorite
        fields = ['product', 'user']


class ReviewSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = ('product', 'user', 'stars', 'comment', 'image')


class ReviewInfoSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    users = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = ('products', 'users', 'stars', 'comment', 'images')

    def get_products(self, obj):
        product = obj.product
        return ProductInfoSerializer(product, many=False, context={"request": self.context["request"]}).data

    def get_users(self, obj):
        user = obj.user
        return UserInfoSerializer(user, many=False, context={"request": self.context["request"]}).data

    def get_images(self, obj):
        images = Image.objects.filter(review=obj).all()
        return ImageReviewSerializer(images, many=True).data


class NotificationSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    class Meta:
        model = Notification
        fields = ['title', 'created_at', 'type', ]

    def get_type(self, obj):
        notification_type = obj.type
        return NotificationTypeSerializer(notification_type, many=False,
                                          context={"request": self.context["request"]}).data


class NotificationTypeSerializer(serializers.ModelSerializer):
    unread = serializers.SerializerMethodField()

    class Meta:
        model = NotificationType
        fields = "__all__"

    def get_unread(self, obj):
        uread = UserRead.objects.filter(notification=obj, user=self.context["request"].user).first()
        print(uread)
        if uread:
            count = Notification.objects.filter(type=obj, created_at__gt=uread.read_date).count()
        else:
            count = Notification.objects.filter(type=obj).count()
        return count


class CartSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    users = serializers.SerializerMethodField()

    class Meta:
        model = Cart
        fields = "__all__"

    def get_products(self, obj):
        product = obj.product
        if obj.product:
            return obj.product.name
        return {
            ProductInfoSerializer(product, many=False, context={"request": self.context["request"]}).data
        }

    def get_users(self, obj):
        user = obj.user
        if obj.user:
            return obj.user.username
        return {
            ProductInfoSerializer(user, many=False, context={"request": self.context["request"]}).data
        }


class OrderSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ['user', 'items']

    def get_user(self, obj):
        return obj.user.username

    def get_items(self, obj):
        items = OrderItem.objects.filter(order=obj).all()
        return OrderItemSerializer(items, many=True, context={"request": self.context["request"]}).data


class OrderItemSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = "__all__"

    def get_products(self, obj):
        product = obj.product
        if obj.product:
            return obj.product.name
        return {
            'data':ProductInfoSerializer(product, many=False, context={"request": self.context["request"]}).data
        }
