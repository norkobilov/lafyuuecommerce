import datetime
import decimal

from django.http import HttpResponse
from rest_framework import viewsets, filters, status
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from lafyuu.api.filters import *
from lafyuu.api.pagination import LargeResultsSetPagination
from lafyuu.models import *
from lafyuu.api.serializers import *


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [AllowAny]
    filterset_class = ProductFilter

    # filter_backends = (filters.SearchFilter,)

    @action(detail=True, methods=["POST", "GET"])
    def favorites(self, request,*args, **kwargs):
        try:
            product = self.get_object()
            favorite = Favorite.objects.filter(user=request.user, product=product).first()
            if favorite:
                favorite.delete()
                return Response({"msg": "Delete product successful to Favorite list!"})
            else:
                Favorite.objects.create(user=request.user, product=product)
            return Response({"msg": "Add product successful to Favorite list!"})
        except KeyError:
            return Response({"msg": "KeyError: Something is wrong for add product to Favorite list!"})


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [AllowAny]
    parser_classes = (MultiPartParser, FormParser)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [AllowAny]
    search_fields = ['name', 'season_type']
    filter_backends = (filters.SearchFilter,)
    pagination_class = LargeResultsSetPagination


class FavoriteViewSet(viewsets.ViewSet):

    def list(self, request):
        queryset = Favorite.objects.all()
        serializer = FavoriteSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def product_list(request):
    if request.method == 'GET':
        products = Products.objects.all()
        product_serializer = ProductInfoSerializer(products, many=True)
        return JSONResponse(product_serializer.data)
    elif request.method == 'POST':
        product_serializer = ProductsSerializer(data=request.data)
        if product_serializer.is_valid():
            product_serializer.save()
            return JSONResponse(product_serializer.data, status=status.HTTP_201_CREATED)
        return JSONResponse(product_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([AllowAny])
def product_detail(request, pk):
    try:
        product = Products.objects.get(pk=pk)
    except Products.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        product_serializer = ProductSerializer(product, context={"request": request})
        return Response(product_serializer.data)
    elif request.method == 'PUT':
        product_serializer = ProductSerializer(product, data=request.data, context={"request": request})
        if product_serializer.is_valid():
            product_serializer.save()
            return Response(product_serializer.data)
        return Response(product_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewInfoSerializer
    permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser, FormParser,)

    def create(self, request, *args, **kwargs):
        data = request.data
        data["user"] = request.user.id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        review = serializer.save()
        headers = self.get_success_headers(serializer.data)
        images = request.FILES.getlist('images')
        for f in images:
            review.image.create(image=f).save()
        return Response(ReviewInfoSerializer(review, many=False, context={"request": request}).data,
                        status=status.HTTP_201_CREATED, headers=headers)


@api_view(['GET', ])
@permission_classes([IsAuthenticated])
def notification(request, pk):
    try:
        notification = Notification.objects.get(pk=pk)
        notification_serializer = NotificationSerializer(notification, context={"request": request})
        return Response(notification_serializer.data)
    except Notification.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = NotificationSerializer
    filterset_class = NotificationFilter

    def get_queryset(self):
        if self.action == "list":
            type = self.request.GET.get("type")
            type_object = NotificationType.objects.filter(pk=type).first()
            if type_object:
                uread = UserRead.objects.filter(user=self.request.user, notification=type_object).first()
                if uread:
                    uread.read_date = datetime.datetime.now()
                    uread.save()
                else:
                    UserRead.objects.create(user=self.request.user, notification=type_object,
                                            read_date=datetime.datetime.now())
        return Notification.objects.all()


class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = OrderItemSerializer

    def create(self, request, *args, **kwargs):
        try:
            user = request.user
            carts = Cart.objects.filter(user_id=user)
            if carts.exists():
                order = Order.objects.create(user=user)
                total_price = 0
                for cart in carts:
                    if cart:
                        if cart.product.sale:
                            sale_price = float(cart.product.sale.stock)
                            order_item = OrderItem.objects.create(order=order,
                                                                  product=cart.product,
                                                                  quantity=cart.quantity,
                                                                  price=(
                                                                      float(cart.product.price) * (sale_price) / 100) * (
                                                                      cart.quantity)
                                                                  )
                            total_price += order_item.price
                        else:
                            order_item = OrderItem.objects.create(order=order,
                                                                  product=cart.product,
                                                                  quantity=cart.quantity,
                                                                  price=float(cart.product.price) * float(cart.quantity)
                                                                  )
                            total_price += order_item.price

                order.price = total_price
                order.save()
                carts.delete()

                res = {
                    'status': 1,
                    'order': OrderSerializer(order, many=False, context={"request": request}).data
                }
                return Response(res)
            return Response("Cart is empty!")
            # return Response(OrderItemSerializer(carts, many=False, context={"request": request}).data,)
        except KeyError:
            res = {
                'status': 0,
                'msg': 'error'
            }
            return Response(res)


class OrderViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer

    @action(detail=False, methods=["POST", "GET"])
    def order_item(self, request):
        user = request.user
        pk = request.data['product']
        quantity = request.data['quantity']
        products = OrderItem.objects.filter(product=pk).first()
        product = Products.objects.filter(id=pk).first()
        if products is None:
            if product:
                order = Order.objects.create(user=user)
                total_price = 0
                order_item = OrderItem.objects.create(order=order,
                                                      product=product,
                                                      quantity=quantity,
                                                      price=float(product.price) * float(quantity)
                                                      )
                total_price += order_item.price
                order_item.price = total_price
                order_item.save()
                res = {
                    'status': 1,
                    'order': OrderSerializer(order, many=False, context={"request": request}).data
                }
                return Response(res)
            return HttpResponse("ok")
        products.quantity = request.data.get("quantity")
        products.price = float(products.quantity) * float(product.price)
        products.save()
        return Response(OrderItemSerializer(products, many=False, context={"request": request}).data,
                        status=status.HTTP_201_CREATED, )


class CartViewSet(viewsets.ModelViewSet):
    queryset = Cart.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = CartSerializer

    def get_queryset(self):
        return Cart.objects.filter(user=self.request.user).all()

    def create(self, request, *args, **kwargs):
        user_id = request.user.id
        pk = request.data['product']
        quantity = request.data['quantity']

        f = Cart.objects.filter(product_id=pk, user_id=user_id).first()
        if f is None:
            data = {
                'user': user_id,
                'product': pk,
                'quantity': quantity,
            }
            serializer = CartSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            f.quantity = request.data.get("quantity")
            f.save()
            serializer = self.get_serializer(f)
            return Response(serializer.data, )
