import django_filters
from lafyuu.models import *
from django_filters import CharFilter, NumberFilter


class ProductFilter(django_filters.FilterSet):
    name = CharFilter(lookup_expr='icontains')
    price__gt = django_filters.NumberFilter(field_name='price', lookup_expr='gt', )
    price__lt = django_filters.NumberFilter(field_name='price', lookup_expr='lt', )
    sale = django_filters.ModelChoiceFilter(queryset=Sale.objects.all())

    class Meta:
        model = Products
        fields = ['name', 'price__gt', 'price__lt', 'sale', ]


class CategoryFilter(django_filters.FilterSet):
    name = CharFilter(lookup_expr='icontains')

    class Meta:
        model = Category
        fields = ['name', 'season_type']


class NotificationFilter(django_filters.FilterSet):
    type = django_filters.ModelChoiceFilter(queryset=NotificationType.objects.all())

    class Meta:
        model = Notification
        fields = ['type']
