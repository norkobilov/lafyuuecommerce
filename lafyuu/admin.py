from django.contrib import admin
from lafyuu.models import *
# Register your models here.
class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('date_order',)

admin.site.register(Category)
admin.site.register(Cart)
admin.site.register(NotificationType)
admin.site.register(Notification)
admin.site.register(UserRead)
admin.site.register(Products)
admin.site.register(Size)
admin.site.register(Colour)
admin.site.register(Favorite)
admin.site.register(Review)
admin.site.register(Dislike)
admin.site.register(Like)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(Address)
admin.site.register(ShippingAddress)
admin.site.register(Image)
admin.site.register(Sale)
