from django.db import models
from accounts.models import CustomUser


# Create your models here.
class Category(models.Model):
    SEASON_TYPE = (
        ('Spring', 'Spring'),
        ('Summer', 'Summer'),
        ('Autumn', 'Autumn'),
        ('Winter', 'Winter'),
        ('General', 'General')
    )
    name = models.CharField(null=True, blank=True, max_length=50)
    season_type = models.CharField(max_length=50, null=True, blank=True, choices=SEASON_TYPE)
    parent = models.ForeignKey('self', related_name='children', null=True, blank=True, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class NotificationType(models.Model):
    title = models.CharField(null=True, blank=True, max_length=50)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Notification(models.Model):
    title = models.CharField(null=True, blank=True, max_length=50)
    type = models.ForeignKey(NotificationType, related_name='notification', on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class UserRead(models.Model):
    user = models.ForeignKey(CustomUser, related_name='user_read', on_delete=models.CASCADE)
    notification = models.ForeignKey(NotificationType, related_name='user_read', on_delete=models.CASCADE, null=True,
                                     blank=True)
    read_date = models.DateTimeField()

    def __str__(self):
        return self.user.username


class Products(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    sale = models.ForeignKey('Sale', related_name='product', on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    colour = models.ManyToManyField('Colour', related_name='product', null=True, blank=True)
    size = models.ManyToManyField('Size', related_name='product', null=True, blank=True)
    category = models.ForeignKey(Category, related_name='product', null=True, blank=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)
        # return str(self.name) + ": $" + str(self.price)


class Sale(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    stock = models.CharField(max_length=5, null=True, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    def __str__(self):
        return self.name


class Size(models.Model):
    size = models.CharField(max_length=5, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.size


class Colour(models.Model):
    name = models.CharField(max_length=10, null=True, blank=True)
    code = models.CharField(max_length=10, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Favorite(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.product.name)


class Image(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to='Products', default='/favicon.png')
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.image)


class Review(models.Model):
    comment = models.TextField()
    stars = models.IntegerField()
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Products, on_delete=models.CASCADE, null=True)
    image = models.ManyToManyField(Image, null=True, blank=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    replay = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name="comment_replay")
    like_count = models.IntegerField(default=0)
    dislike_count = models.IntegerField(default=0)

    def __str__(self):
        return str(self.comment)


class Like(models.Model):
    user = models.ForeignKey(CustomUser, related_name='userlike', null=True, blank=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, related_name='productlike', null=True, blank=True, on_delete=models.CASCADE)
    comment = models.ForeignKey(Review, related_name='reviewlike', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        if self.product:
            return str(self.product.name)
        elif self.comment:
            return str(self.comment.comment)
        elif self.user:
            return str(self.user.first_name)
        return (self.id)


class Dislike(models.Model):
    user = models.ForeignKey(CustomUser, related_name='userdislike', null=True, blank=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, related_name='productdislike', null=True, blank=True,
                                on_delete=models.CASCADE)
    comment = models.ForeignKey(Review, related_name='reviewdislike', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        if self.product:
            return str(self.product.name)
        elif self.comment:
            return str(self.comment.comment)
        elif self.user:
            return str(self.user.first_name)
        return (self.id)


class Order(models.Model):
    Status_Type = (
        ('Packing', 'Packing'),
        ('Shipping', 'Shipping'),
        ('Arriving', 'Arriving'),
        ('Success', 'Success')
    )
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, blank=True, null=True, related_name='order')
    date_order = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, null=True, choices=Status_Type)

    class Meta:
        ordering = ('-date_order',)

    def __str__(self):
        return f'{self.user} - {str(self.id)}'

    def get_total_price(self, obj):
        items = OrderItem.objects.filter(order=obj).all()
        total_sum = 0
        for item in items:
            total_sum += item.quantity * item.price
        return  total_sum

class OrderItem(models.Model):
    product = models.ForeignKey(Products, related_name='order_item', on_delete=models.SET_NULL, blank=True, null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    order = models.ForeignKey(Order, related_name='item', on_delete=models.SET_NULL, blank=True, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.order.user.username) + "<--->" + str(self.product.name)
        # return  str(self.product.name)

    def get_cost(self):
        return self.price * self.quantity


class Cart(models.Model):
    user = models.ForeignKey(CustomUser, related_name='cart', null=True, blank=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, related_name='cart', null=True, blank=True, on_delete=models.CASCADE)
    quantity = models.IntegerField(blank=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user.username) + ' <---> ' + str(self.product.name)


class Address(models.Model):
    country = models.CharField(max_length=100)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    street_address = models.CharField(max_length=30)
    street_address2 = models.CharField(max_length=30, null=True, blank=True)
    city = models.CharField(max_length=30, null=True)
    region = models.CharField(max_length=30, null=True)
    zip_code = models.CharField(max_length=30, null=True)
    phone_number = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.country


class ShippingAddress(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)

    def __str__(self):
        return self.id
