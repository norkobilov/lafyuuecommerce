from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class CustomUser(AbstractUser):
    GENDER_TYPE = (
        (1, 'Male'),
        (2, 'Female'),
        (2, 'Other'),
    )
    birth_date = models.DateField(null=True, blank=True)
    gender = models.IntegerField(choices=GENDER_TYPE, default=1, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)
    sms_code = models.IntegerField(default=0, null=True, blank=True)
    status = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return self.username


class BlockList(models.Model):
    token = models.CharField('User Block List', max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.token


class UserDevice(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    token = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.user)
