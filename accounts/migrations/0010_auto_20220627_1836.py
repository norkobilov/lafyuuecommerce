# Generated by Django 3.2 on 2022-06-27 13:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_auto_20220627_1802'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdevice',
            old_name='access_token',
            new_name='token',
        ),
        migrations.RemoveField(
            model_name='userdevice',
            name='refresh_token',
        ),
    ]
