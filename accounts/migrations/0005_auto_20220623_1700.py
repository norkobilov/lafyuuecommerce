# Generated by Django 3.1.2 on 2022-06-23 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20220623_1541'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdevice',
            name='last_login',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='userdevice',
            name='token',
            field=models.CharField(max_length=80),
        ),
    ]
