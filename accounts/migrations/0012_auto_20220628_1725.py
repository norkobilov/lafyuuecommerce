# Generated by Django 3.1.2 on 2022-06-28 12:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_auto_20220628_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blocklist',
            name='token',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='User Block List'),
        ),
        migrations.AlterField(
            model_name='userdevice',
            name='token',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
