from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from django.utils import timezone

timezone.activate("Asia/Tashkent")

from accounts.models import *


# Register your models here.

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ['username', 'email', 'first_name', 'phone', 'sms_code', 'gender', 'is_staff', 'status', ]
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ['phone', 'sms_code']}),
    )


@admin.register(UserDevice)
class UserDeviseAdmin(admin.ModelAdmin):
    readonly_fields = ['created_at', 'updated_at', ]

    def add_view(self, request, form_url='', extra_context=None):
        timezone.activate("Asia/Tashkent")
        return super(UserDeviseAdmin, self).add_view(
            request,
            form_url,
            extra_context
        )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        timezone.activate("Asia/Tashkent")
        return super(UserDeviseAdmin, self).change_view(
            request,
            object_id,
            form_url,
            extra_context
        )

    def get_time(self, time):
        import pytz
        fmt = '%Y-%m-%d %H:%M:%S %Z'
        tz = pytz.timezone("Asia/Tashkent")
        dt = time.astimezone(tz)
        return dt.strftime(fmt)

    def created_at_etc(self, custom_user):
        return self.get_time(custom_user.created_at)

    created_at_etc.short_description = "Created At"


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(BlockList)
