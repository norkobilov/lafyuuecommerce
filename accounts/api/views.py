import random
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.http import JsonResponse
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from accounts.api.serializers import RegisterSerializer
from accounts.models import CustomUser, UserDevice, BlockList
from config.settings import EMAIL_HOST_USER
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes, api_view
from rest_framework_simplejwt.tokens import BlacklistMixin, RefreshToken, AccessToken
from accounts.api.serializers import CustomUserSerializer, ChangePasswordSerializer


class AccessRefreshToken(BlacklistMixin, AccessToken):
    pass


@permission_classes([IsAuthenticated])
@api_view(["GET"])
def user(request):
    user = request.user
    user_data = CustomUserSerializer(user, many=False).data
    return JsonResponse({"user_data": user_data})


@permission_classes([IsAuthenticated])
@api_view(["POST"])
def log_out(self, request):
    try:
        refresh_token = request.data["refreshToken"]
        print("*********")
        print(refresh_token)
        token = RefreshToken(refresh_token)
        token.blacklist()

        return Response(status=status.HTTP_205_RESET_CONTENT)
    except Exception as e:
        return Response(status=status.HTTP_400_BAD_REQUEST)


class RegisterViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = [AllowAny]

    @action(detail=False, methods=["POST"])
    def register_user(self, request):
        try:
            username = request.POST["username"]
            first_name = request.POST["first_name"]
            last_name = request.POST["last_name"]
            email = request.POST["email"]
            password = request.POST["password"]
            if CustomUser.objects.filter(username=username).exists():
                return JsonResponse({"msg": "A user with that username already exists.", })
            user = CustomUser.objects.create_user(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password
            )
            user.password = make_password(password)
            user.save()
            return JsonResponse({"status": user.status, "data": {"username": user.username}})
        except KeyError:
            return JsonResponse({"status": 0, "msg": "Some data is wrong for register !"})

    @action(detail=False, methods=["POST"])
    def login_user(self, request):
        try:
            username = request.POST["username"]
            password = request.POST["password"]
            user = CustomUser.objects.filter(username=username).first()
            if user:
                if user.check_password(password):
                    refreshToken = RefreshToken.for_user(user)
                    accessToken = refreshToken.access_token
                    user.status = 1
                    user.save()
                    udevice = UserDevice.objects.filter(user=user).first()
                    if udevice:
                        BlockList.objects.create(
                            token=udevice.token
                        )
                        udevice.token = accessToken
                        udevice.save()
                    else:
                        user_device = UserDevice.objects.create(
                            user=user,
                            token=accessToken
                        )
                        user_device.save()
                    return Response({
                        'status': user.status,
                        'refresh': str(refreshToken),
                        'access': str(accessToken),

                    })
                return JsonResponse({"msg": "Oops! Your Password Is Not Correct"})
            return JsonResponse({"msg": "A user with that username doesn't exists."})
        except KeyError:
            return JsonResponse({"status": 0, "msg": "KeyError: Some data is wrong for login !"})

    @action(detail=False, methods=["POST"])
    def forget_password(self, request):
        try:
            username = request.POST["username"]
            user = CustomUser.objects.filter(username=username).first()
            if user:
                sms_code = random.randint(1000, 9999)
                recipient = user.email
                subject = 'PyDevService LLC'
                message = 'Code Reset To Password:' + str(sms_code)
                send_mail(subject, message, EMAIL_HOST_USER, [recipient], fail_silently=False)
                user.sms_code = sms_code
                user.status = 0
                user.save()
                return JsonResponse(
                    {"status": 0, "msg": "Private Code has been sent successfully to recovery your password."}
                )
            return JsonResponse({"msg": "A user with that username doesn't exists.", })
        except KeyError:
            return JsonResponse({"status": 0, "msg": "KeyError: Something is wrong for update password!"})

    @action(detail=False, methods=["POST"])
    def update_password(self, request, *args, **kwargs):
        try:
            username = request.POST["username"]
            new_password = request.POST["new_password"]
            sms_code = request.POST["sms_code"]

            user = CustomUser.objects.filter(username=username).first()
            if user and user.sms_code == int(sms_code):
                user.status = 1
                user.set_password(new_password)
                user.save()

            return JsonResponse({"msg": "Username or sms_code is wrong!"})
        except KeyError:
            return JsonResponse({"status": 0, "msg": "KeyError: Something is wrong for update password!"})


class ChangePasswordViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = ChangePasswordSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=False, methods=["POST"])
    def change_password(self, request, *args, **kwargs):
        try:
            serializer = ChangePasswordSerializer(data=request.POST, context={'request': request})
            serializer.is_valid(raise_exception=True)
            serializer.validate_old_password(value=serializer.validated_data['old_password'])
            serializer.update(instance=request.user, validated_data=serializer.validated_data)
            return JsonResponse({"staus": 1})

        except KeyError:
            return JsonResponse({"status": 0, "msg": "KeyError: Something is wrong for update password!"})

