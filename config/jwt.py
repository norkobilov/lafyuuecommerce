import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings
import jwt
from django.utils.translation import ugettext as _
from rest_framework import exceptions
from accounts.models import BlockList
from rest_framework_simplejwt.authentication import JWTAuthentication
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

from rest_framework import status


class CustomException(exceptions.AuthenticationFailed):
    status_code = status.HTTP_401_UNAUTHORIZED


class CustomExceptionMobile(exceptions.AuthenticationFailed):
    status_code = 431


class JSONCustomWebTokenAuthentication(JWTAuthentication):
    def authenticate(self, request):
        headers = request.headers if hasattr(request, 'headers') else {}
        platform = headers['platform'] if 'platform' in headers else None

        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        validated_token = self.get_validated_token(raw_token)
        if BlockList.objects.filter(token=validated_token).exists():
            msg = _('Token black list add.')
            if platform == "android":
                raise CustomExceptionMobile(msg)
            elif platform == "ios":
                raise CustomExceptionMobile(msg)
            raise CustomException(msg)
        return self.get_user(validated_token), validated_token

