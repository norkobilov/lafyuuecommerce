"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from accounts.api.views import user, log_out

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenBlacklistView,
)
from rest_framework import routers
from accounts.api.views import RegisterViewSet, ChangePasswordViewSet
from lafyuu.api.views import *
from django.conf import settings
from django.conf.urls.static import static
from click.views import ClickUzMerchantAPIView

routers = routers.DefaultRouter()
routers.register(r'register', RegisterViewSet)
routers.register(r'password', ChangePasswordViewSet)
routers.register(r'category', CategoryViewSet)
routers.register(r'product', ProductViewSet)
routers.register(r'image', ImageViewSet)
routers.register(r'favorite', FavoriteViewSet, basename='favorite')
routers.register(r'notification', NotificationViewSet, basename='notification')
routers.register(r'review', ReviewViewSet,)
routers.register(r'order-item', OrderItemViewSet, basename='order-item')
routers.register(r'order-item-to', OrderViewSet, basename='order')
routers.register(r'cart', CartViewSet,)
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user', user),
    path('log_out', log_out),
    path('product_list/', product_list),
    path('notification/<int:pk>', notification),
    path('product_detail/<int:pk>', product_detail),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/blacklist/', TokenBlacklistView.as_view(), name='token_blacklist'),
    path('click/', ClickUzMerchantAPIView.as_view(), name='click'),

]
urlpatterns += routers.urls
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
